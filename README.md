# BilloWars

Dieses Plugin wurde geschrieben um BedWars in der 1.16 für **Projekt Titanium** zu testen.

Wegen der unverschämt wenigen Features heißt dieses Minigame (sofern man es denn überhaupt so nennen kann) **BilloWars**.

[DOWNLOAD*](https://gitlab.com/Floppy012/billowars/builds/artifacts/master/download?job=Build)

`* Wenn hier jemand zu blöd ist seinen PC zu bedienen, dann einfach lassen. Ich hafte für nix.` 

## Map Setup
Wenn man das Plugin installiert hat und den Server das erste Mal startet, wird eine neue Welt mit dem Namen "billowars" generiert.

Diese Map ist vollkommen leer und dient als Bauplatz für eine Bedwars Map. Die Map sollte am Ende mindestens folgendes Besitzen: 

- Eine Mitte (für einen Hauptspawn)
- Eine Plattform pro Team
    - Man kann auch nur eine Plattform bauen, macht aber wenig Sinn ...
- Jede Plattform sollte ein Bett besitzen

### Auf die Map teleportieren
Um die Einrichtung einer Map zu beginnen teleportiert man sich mit folgendem Befehl auf die Map:  
`/setup teleport`

### Zentraler Spawn
Der Zentrale Spawn dient für Spieler die kein Team gewählt haben.

1. An die gewünschte Stelle stellen und in die gewünschte Richtung schauen
2. `/setup spawn`

### Team Spawn
Ähnlich wie der Zentrale Spawn wird auch der Spawn pro Team festgelegt.

1. An die gewünschte Stelle Stellen und in die gewünschte Richtung schauen
2. `/setup spawn <teamname>`

`<teamname>` sollte (muss aber nicht) eine Farbe auf Englisch repräsentieren (z.B. red, green, blue, usw.)

**Hinweis:** Wenn unter dem angegebenen Teamnamen noch kein Team existiert, wird dieses erstellt!

### Team Bett

1. Auf das Bett des Teams stellen
2. `/setup bed <teamname>`

`<teamname>` sollte (muss aber nicht) eine Farbe auf Englisch repräsentieren (z.B. red, green, blue, usw.)

**Hinweis:** Wenn unter dem angegebenen Teamnamen noch kein Team existiert, wird dieses erstellt!

### Inventar
Um das fehlende Shop-Feature auszugleichen bekommt jeder Spieler zu Beginn und nach einem Tod ein Standard-Inventar zugewiesen.

Um dieses Inventar festzulegen, leert man zuerst sein eigenes Inventar und packt die gewünschten Items an die gewünschte Position

Abschließend wird mit dem Befehl `/setup inventory` das Inventar übernommen.

### Friendly Fire (Optional)

Über den Befehl `/setup ff <teamname>` kann pro team das friendly fire getoggled werden.

### Speichern

Wenn alle der obigen Schritte abgearbeitet wurden ("Team"-Schritte können logischerweise mehrfach angewandt werden), dann kann die Konfiguration
gespeichert werden.

Dies wird über den Befehl `/setup save` getan. Sollte irgendwas nicht passen, bekommt man eine Nachricht über das was nicht passt.

Die Welt wird anschließend ebenfalls gespeichert und neu geladen.

Die Map ist nun Spielbereit.

## Spielen

### Teams beitreten
Teams haben keine Obergrenze. Jeder Spieler der spielen möchte wählt ein Team aus.

`/bw join <teamname>`

**Hinweis:** Um herauszufinden welche Teams existieren kann der Befehl `/bw teams` verwendet werden.

### Spiel starten
Wenn alle Spieler ihrem Wunschteam beigetreten sind, kann das Spiel mit `/bw start` gestartet werden.

- Das Spiel hat keinen Timer und hört auch nicht automatisch auf, wenn alle Teams bis auf eins eliminiert wurden.
- Man kann alles abbauen (auch das eigene Bett)
- Betten droppen ihre Items man kann sie also wieder hinbauen ¯\\\_(ツ)\_\/¯
- Spieler die keinem Team beigetreten sind werden Spectator
- Spieler die sterben während das Bett zerstört ist, werden Spectator
- Spectator können jederzeit ins Spiel zurück mit `/bw teleport`

### Spiel stoppen
Hat man genug von der shit show kann das Spiel wieder mit `/bw stop` gestoppt werden. Die Map wird ohne zu speichern neu geladen, sodass direkt das nächste Spiel starten kann
um den ganzen Tag puren Spielspaß zu erleben. 

## Map sharing
Die Config zu einer Map wird im Map-Ordner gespeichert. Maps können also easy per Drag&Drop verteilt werden.

## Bugs

Wer zu faul ist, hier ein Issue aufzumachen kann seinen Bug behalten.

## Contributing

Jo

## Lizenz

[WTFPL](http://www.wtfpl.net/)