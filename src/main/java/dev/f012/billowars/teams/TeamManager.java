package dev.f012.billowars.teams;

import dev.f012.billowars.BilloWars;
import dev.f012.billowars.GameConfig;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;


/**
 * Manager for managing {@link Scoreboard} {@link Team}s
 */
public class TeamManager {

  private final GameConfig config;
  private final Scoreboard scoreboard;

  /**
   * Instantiates this manager, creates a new scoreboard and adds the configured teams.
   * @param config the game configuration.
   */
  public TeamManager(GameConfig config) {
    this.config = config;
    this.scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
    this.config.teams.forEach((name, configTeam) -> {
      Team team = this.scoreboard.registerNewTeam(name.toLowerCase());
      team.setColor(getColor(name));
      team.setAllowFriendlyFire(configTeam.friendlyFire);
      team.setCanSeeFriendlyInvisibles(false);
      team.setOption(Team.Option.COLLISION_RULE, Team.OptionStatus.NEVER);
      team.setOption(Team.Option.DEATH_MESSAGE_VISIBILITY, Team.OptionStatus.ALWAYS);
    });
  }

  /**
   * Sets a player's team
   * @param name the team name
   * @param player the player to add to the team.
   * @return whether the player was added to the team.
   */
  public boolean setTeam(String name, Player player) {
    player.setScoreboard(this.scoreboard);
    Team team = this.scoreboard.getTeam(name);
    if (team == null) {
      return false;
    }

    team.addEntry(player.getName());
    return true;
  }

  /**
   * Gets the team name that a player is in.
   * @param player the player to get the team name of.
   * @return the team name or <i>null</i> if the player is not member of a team.
   */
  public String getTeam(Player player) {
    Team t = this.scoreboard.getEntryTeam(player.getName());
    if (t == null) return null;
    return t.getName();
  }

  /**
   * Gets the spawn location of a player based on its team.
   * @param player the player to get the spawn location of.
   * @return the players teams sawn location or <i>null</i> if the player is not member of a team.
   */
  public GameConfig.SimpleLocation getSpawnLocation(Player player) {
    String teamName = this.getTeam(player);
    if (teamName == null) return null;
    GameConfig.GameTeam configTeam = this.config.teams.get(teamName);

    return configTeam.spawn;
  }

  /**
   * Gets the bed location of a players team.
   * @param player the player to get its teams bed location.
   * @return the bed location or <i>null</i> if the player is not member of a team.
   */
  public GameConfig.SimpleLocation getBedLocation(Player player) {
    String teamName = this.getTeam(player);
    if (teamName == null) return null;
    GameConfig.GameTeam configTeam = this.config.teams.get(teamName);

    return configTeam.bed;
  }

  /**
   * Removes all players from the scoreboard of this manager.
   */
  public void destroy() {
    Scoreboard main = Bukkit.getScoreboardManager().getMainScoreboard();
    for (Player player : Bukkit.getOnlinePlayers()) {
      player.setScoreboard(main);
    }
  }

  /**
   * Tries to get a color based on the team name.
   * @param name the team name (red, blue, green, ...)
   * @return the according color or {@link ChatColor#WHITE} if no color could be found by the team name.
   */
  private static ChatColor getColor(String name) {
    try {
      return ChatColor.valueOf(name.toUpperCase());
    } catch (IllegalArgumentException e) {
      BilloWars.getInstance().getLogger().warning("Cannot parse color from team name " + name);
      return ChatColor.WHITE;
    }
  }

}
