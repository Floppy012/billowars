package dev.f012.billowars;

import dev.f012.billowars.teams.TeamManager;
import dev.f012.billowars.util.BlockUtils;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/**
 * Manager for in-game processes.
 */
public class GameManager {

  @Getter
  private final GameConfig config;

  @Getter
  private final World gameWorld;

  @Getter
  private final TeamManager teamManager;

  @Getter
  private boolean started;


  /**
   * Instantiates this manager.
   * @param config the game config
   * @param gameWorld the game world
   */
  public GameManager(GameConfig config, World gameWorld) {
    this.config = config;
    this.gameWorld = gameWorld;
    this.teamManager = new TeamManager(config);
  }

  /**
   * Starts the game
   */
  public void start() {
    if (this.started) return;
    this.started = true;
    Bukkit.getOnlinePlayers().forEach((player) -> this.respawnPlayer(player, false));
    Bukkit.broadcastMessage("§6Game started!");
  }

  /**
   * Ends the game
   */
  public void stop() {
    if (!this.started) return;
    World defaultWorld = Bukkit.getWorlds().get(0);
    Bukkit.getOnlinePlayers().forEach((player) -> {
      player.getInventory().clear();
      player.setGameMode(GameMode.CREATIVE);
      player.setAllowFlight(true);
      player.teleport(defaultWorld.getSpawnLocation());
    });
    Bukkit.broadcastMessage("§cGame stopped!");
    BilloWars.getInstance().reloadWorld(false);
    this.started = false;
  }

  /**
   * Handles a player death.
   * @param player the player that died.
   */
  public void onPlayerDeath(Player player) {
    this.respawnPlayer(player, !this.isBedUp(player));
  }

  /**
   * Respawns a player at its teams spawn location. If the player is not in a team
   * the player is respawned at the default spawn defined in the game configuration.
   * @param player the player to respawn
   * @param spectator whether to respawn the player in spectator mode
   */
  public void respawnPlayer(Player player, boolean spectator) {
    GameConfig.SimpleLocation simpleSpawnLoc = this.teamManager.getSpawnLocation(player);
    Location teamLoc = simpleSpawnLoc == null
        ? this.config.spawn.toLocation(this.gameWorld)
        : simpleSpawnLoc.toLocation(this.gameWorld);

    if (this.started && !spectator) {
      Inventory inv = player.getInventory();
      inv.setContents(this.getDefaultInventory());
    }

    player.setGameMode(spectator ? GameMode.SPECTATOR : GameMode.SURVIVAL);
    if (spectator) {
      player.setAllowFlight(true);
      player.setFlying(true);
    } else {
      player.setFlying(false);
      player.setAllowFlight(false);
    }

    player.teleport(teamLoc);
    player.setHealth(20D);
  }

  /**
   * Whether the bed of a players team is sill there.
   * @param player the player to check the teams bed of.
   * @return true if the bed is still there, false otherwise.
   */
  public boolean isBedUp(Player player) {
    GameConfig.SimpleLocation simpleBedLoc = this.getTeamManager().getBedLocation(player);
    if (simpleBedLoc == null) return false;
    Block bedBlock = simpleBedLoc.toLocation(this.gameWorld).getBlock();
    return BlockUtils.isBed(bedBlock);
  }

  /**
   * Gets a clone of the default inventory.
   * @return the clone of the inventory.
   */
  private ItemStack[] getDefaultInventory() {
    ItemStack[] out = new ItemStack[this.config.inventory.length];
    ItemStack[] in = this.config.getCachedInventory();

    for (int i = 0; i < out.length; i++) {
      ItemStack source = in[i];
      if (source == null) continue;

      out[i] = source.clone();
    }

    return out;
  }

}
