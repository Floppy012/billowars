package dev.f012.billowars.command;


import dev.f012.billowars.BilloWars;
import dev.f012.billowars.GameConfig;
import dev.f012.billowars.GameManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 * BilloWars command for managing the game.
 */
public class BilloWarsCommand extends BaseCommand {

  public BilloWarsCommand() {
    super.preCheck = this::preCheck;
    super.handlers.put("teleport", this::teleport);
    super.handlers.put("teams", this::getTeams);
    super.handlers.put("join", this::join);
    super.handlers.put("start", this::start);
    super.handlers.put("stop", this::stop);
  }

  /**
   * Checks if this command may be executed based on whether the game is configured or not.
   */
  private boolean preCheck(Player player) {
    if (!BilloWars.getInstance().isGameReady()) {
      player.sendMessage("§cNo world configured!");
      return false;
    }

    return true;
  }


  /**
   * Starts the game
   * @see GameManager#start()
   */
  private boolean start(Player player, String[] args) {
    GameManager gm = BilloWars.getInstance().getGameManager();
    if (gm.isStarted()) {
      player.sendMessage("§cGame already started!");
      return true;
    }

    gm.start();
    return true;
  }

  /**
   * Ends the game
   * @see GameManager#stop()
   */
  private boolean stop(Player player, String[] args) {
    GameManager gm = BilloWars.getInstance().getGameManager();
    if (!gm.isStarted()) {
      player.sendMessage("§cGame not started!");
      return true;
    }

    gm.stop();
    return true;
  }

  /**
   * Teleports the executing player or all players onto the game map.
   */
  private boolean teleport(Player player, String[] args) {
    GameManager gm = BilloWars.getInstance().getGameManager();
    if (args.length == 0) {
      gm.respawnPlayer(player, false);
      return true;
    }

    if (args[0].equalsIgnoreCase("all")) {
      Bukkit.getOnlinePlayers().forEach((p) -> gm.respawnPlayer(p, false));
      return true;
    }

    return false;
  }

  /**
   * Zeigt eine Liste der verfügbaren Teams an.
   */
  private boolean getTeams(Player player, String[] args) {
    GameConfig gc = BilloWars.getInstance().getGameConfig();
    player.sendMessage("§bVerfügbare Teams:");
    gc.teams.keySet().forEach((name) -> player.sendMessage("\t- " + name));
    return true;
  }

  /**
   * Puts the executing player into a team.
   */
  private boolean join(Player player, String[] args) {
    if (args.length < 1) {
      return false;
    }

    String teamName = args[0].toLowerCase();
    GameManager gm = BilloWars.getInstance().getGameManager();
    if (!gm.getTeamManager().setTeam(teamName, player)) {
      player.sendMessage("§cTeam not found!");
      return true;
    }

    player.sendMessage("§aSuccessfully joined team " + teamName + "!");
    return true;
  }
}
