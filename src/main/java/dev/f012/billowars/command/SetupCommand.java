package dev.f012.billowars.command;

import dev.f012.billowars.BilloWars;
import dev.f012.billowars.GameConfig;
import dev.f012.billowars.util.BlockUtils;
import dev.f012.billowars.util.ItemStackAdapter;
import org.bukkit.GameMode;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.io.IOException;
import java.util.Map;
import java.util.function.Function;
import java.util.logging.Level;

/**
 * Setup command used for setting up the game map.
 */
public class SetupCommand extends BaseCommand {

  public SetupCommand() {
    super.preCheck = this::preCheck;
    super.handlers.put("teleport", this::teleport);
    super.handlers.put("bed", this::setBed);
    super.handlers.put("spawn", this::setSpawn);
    super.handlers.put("inventory", this::setInventory);
    super.handlers.put("ff", this::setFriendlyFire);
    super.handlers.put("save", this::save);
  }

  /**
   * Checks if this command may be executed based on whether the game is unconfigured.
   */
  private boolean preCheck(Player player) {
    if (BilloWars.getInstance().isGameReady()) {
      player.sendMessage("§cGame already set up!");
      return false;
    }

    return true;
  }

  /**
   * Teleports the executing player onto the game map and sets gamemode to creative and enables flying.
   * Game world might be completely empty.
   */
  private boolean teleport(Player player, String[] args) {
    World gw = BilloWars.getInstance().getGameWorld();
    player.teleport(gw.getSpawnLocation());
    player.setGameMode(GameMode.CREATIVE);
    player.setAllowFlight(true);
    player.setFlying(true);
    return true;
  }

  /**
   * Sets the bed location of a team
   * If the team does not exist it will be created.
   * @see #getTeam(String) 
   */
  private boolean setBed(Player player, String[] args) {
    if (args.length < 1) return false;
    Block bed = player.getLocation().getBlock();

    System.out.println(bed.getType());

    if (!BlockUtils.isBed(bed)) {
      player.sendMessage("§cBlock below you must be bed!");
      return false;
    }

    String teamName = args[0];
    getTeam(teamName).bed = GameConfig.SimpleLocation.fromLocation(bed.getLocation());
    player.sendMessage("§aBed set!");
    return true;
  }

  /**
   * Sets the spawn location of a team
   * If the team does not exist it will be created.
   * @see #getTeam(String)
   */
  private boolean setSpawn(Player player, String[] args) {
    if (args.length < 1) {
      BilloWars.getInstance().getGameConfig().spawn = GameConfig.SimpleLocation.fromLocation(player.getLocation());
      player.sendMessage("§aWorld spawn set!");
      return true;
    }

    String teamName = args[0];
    getTeam(teamName).spawn = GameConfig.SimpleLocation.fromLocation(player.getLocation());
    player.sendMessage("§aSpawn set!");
    return true;
  }

  /**
   * Toggles friendly fire of a team.
   * If the team does not exist it will be created.
   * @see #getTeam(String)
   */
  private boolean setFriendlyFire(Player player, String[] args) {
    if (args.length < 1) return false;

    String teamName = args[0];
    GameConfig.GameTeam team = getTeam(teamName);
    team.friendlyFire = !team.friendlyFire;
    player.sendMessage(
        "§bFriendlyFire for team " + teamName + " is now " + (team.friendlyFire ? "§aenabled" : "§cdisabled")
    );
    return true;
  }

  /**
   * Sets the default inventory that each player gets at the start of a game and after a death.
   */
  private boolean setInventory(Player player, String[] args) {
    ItemStack[] contents = player.getInventory().getContents();
    Map<String, Object>[] serialized = new Map[contents.length];

    for (int i = 0; i < contents.length; i++) {
      ItemStack source = contents[i];
      if (source == null) continue;
      serialized[i] = ItemStackAdapter.serialize(source);
    }

    BilloWars.getInstance().getGameConfig().inventory = serialized;
    player.sendMessage("§aInventory set!");
    return true;
  }

  /**
   * Performs validity checks and if all pass, saves the configuration.
   * After that the world is once saved and unloaded and then re-loaded.
   */
  private boolean save(Player player, String[] args) {
    GameConfig config = BilloWars.getInstance().getGameConfig();

    if (config.spawn == null) {
      player.sendMessage("§cWorld spawn not set!");
      return false;
    }

    if (config.inventory == null) {
      player.sendMessage("§cDefault inventory not set!");
      return false;
    }

    if (config.teams.isEmpty()) {
      player.sendMessage("§cNo teams setup!");
      return false;
    }

    for (Map.Entry<String, GameConfig.GameTeam> entry : config.teams.entrySet()) {
      String name = entry.getKey();
      GameConfig.GameTeam team = entry.getValue();

      if (team.spawn == null) {
        player.sendMessage("§cSpawn not set for team " + name);
        return false;
      }

      if (team.bed == null) {
        player.sendMessage("§cBed not set for team " + name);
        return false;
      }
    }

    try {
      config.save();
    } catch (IOException e) {
      BilloWars.getInstance().getLogger().log(Level.SEVERE, "Could not save game config!", e);
      player.sendMessage("§4Exception while saving config! See console!");
      return true;
    }

    player.sendMessage("§aConfig Saved!");
    BilloWars.getInstance().reloadWorld(true);
    return true;
  }

  /**
   * Gets a team by its name. If the team does not exist it will be created
   * @see Map#computeIfAbsent(Object, Function) 
   */
  private static GameConfig.GameTeam getTeam(String teamName) {
    GameConfig config = BilloWars.getInstance().getGameConfig();
    return config.teams.computeIfAbsent(teamName.toLowerCase(), (name) -> new GameConfig.GameTeam());
  }
}
