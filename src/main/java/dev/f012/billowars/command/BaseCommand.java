package dev.f012.billowars.command;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

/**
 * Simple command handler.
 */
public abstract class BaseCommand implements CommandExecutor {

  /**
   * Sub-Handlers for the command
   */
  protected final Map<String, HandlerMethod> handlers = new HashMap<>();
  /**
   * Function that performs a pre check.
   * Called before any other sub-handler is called.
   * Return <i>false</i> to abort flow.
   */
  protected Function<Player, Boolean> preCheck = null;

  @Override
  public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
    if (args.length == 0) {
      return false;
    }

    if (!(sender instanceof Player)) {
      sender.sendMessage("Only for Players!");
      return false;
    }

    String arg0 = args[0].toLowerCase();
    if (!this.handlers.containsKey(arg0)) {
      return false;
    }

    Player player = (Player) sender;
    if (this.preCheck != null && !this.preCheck.apply(player)) {
      return false;
    }

    String[] newArgs = new String[args.length - 1];
    System.arraycopy(args, 1, newArgs, 0, args.length - 1);
    return handlers.get(arg0).work(player, newArgs);
  }


}
