package dev.f012.billowars.command;

import org.bukkit.entity.Player;

/**
 * Functional interface representing a sub-handler method.
 */
@FunctionalInterface
public interface HandlerMethod {

  boolean work(Player player, String[] args);

}
