package dev.f012.billowars;

import dev.f012.billowars.command.BilloWarsCommand;
import dev.f012.billowars.command.SetupCommand;
import dev.f012.billowars.gen.EmptyChunkGenerator;
import dev.f012.billowars.listener.GameListener;
import dev.f012.billowars.listener.GeneralListener;
import dev.f012.billowars.listener.WorldListener;
import lombok.Getter;
import org.bukkit.*;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;

/**
 * Main class of BILLO WARS.
 */
public class BilloWars extends JavaPlugin {

  private static final String WORLD_NAME = "billowars";

  @Getter
  private static BilloWars instance;

  @Getter
  private World gameWorld;

  @Getter
  private GameConfig gameConfig;

  @Getter
  private GameManager gameManager;


  @Override
  public void onEnable() {
    instance = this;
    this.setupCommands();
    this.setupListeners();
    try {
      this.loadWorld();
    } catch (IOException e) {
      this.getLogger().log(Level.SEVERE, "Error while loading game world!", e);
      this.getServer().shutdown();
    }
  }

  /**
   * @return whether the game is ready (map is setup etc.)
   */
  public boolean isGameReady() {
    return this.gameManager != null;
  }

  /**
   * Registers command executors.
   */
  private void setupCommands() {
    this.getCommand("setup").setExecutor(new SetupCommand());
    this.getCommand("billowars").setExecutor(new BilloWarsCommand());
  }

  /**
   * Registers listeners.
   */
  private void setupListeners() {
    this.getServer().getPluginManager().registerEvents(new GeneralListener(), this);
    this.getServer().getPluginManager().registerEvents(new GameListener(), this);
    this.getServer().getPluginManager().registerEvents(new WorldListener(), this);
  }

  /**
   * Reloads the Game world and resets the game.
   * @param save whether to save the game map.
   */
  public void reloadWorld(boolean save) {
    if (this.gameManager != null) {
      this.gameManager.getTeamManager().destroy();
    }

    World defaultWorld = this.getServer().getWorlds().get(0);
    Bukkit.getOnlinePlayers().stream().filter((player) -> player.getWorld() != defaultWorld).forEach((player) ->
        player.teleport(defaultWorld.getSpawnLocation())
    );

    this.getServer().unloadWorld(this.gameWorld, save);
    try {
      this.loadWorld();
    } catch (IOException e) {
      this.getLogger().log(Level.SEVERE, "Error while loading game world!", e);
      this.getServer().shutdown();
    }

    Bukkit.broadcastMessage("§6World reloaded! Please re-join your teams!");
  }

  /**
   * Loads the Game world and sets up the game.
   * @throws IOException if an error occurred during config loading.
   */
  private void loadWorld() throws IOException {
    File worldDir = new File(this.getServer().getWorldContainer(), WORLD_NAME);
    File gameConfig = new File(worldDir, "config.json");
    boolean gcExists = gameConfig.exists();
    this.gameConfig = gcExists ? GameConfig.load(gameConfig) : GameConfig.empty(gameConfig);

    WorldCreator wc = new WorldCreator("billowars")
        .environment(World.Environment.NORMAL)
        .type(WorldType.FLAT)
        .hardcore(false)
        .generator(new EmptyChunkGenerator());

    this.gameWorld = this.getServer().createWorld(wc);
    if (this.gameWorld == null) {
      throw new RuntimeException("Cannot load world!");
    }

    this.gameWorld.setAutoSave(!gcExists);

    if (gcExists) {
      this.gameManager = new GameManager(this.gameConfig, this.gameWorld);
    }
  }

}
