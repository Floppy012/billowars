package dev.f012.billowars.listener;

import dev.f012.billowars.BilloWars;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.world.ChunkUnloadEvent;

/**
 * Listener for world related events.
 */
public class WorldListener implements Listener {

  @EventHandler
  public void onChunkUnload(ChunkUnloadEvent event) {
    if (BilloWars.getInstance().getGameWorld() != event.getWorld()) return;
    event.setSaveChunk(!BilloWars.getInstance().isGameReady());
  }

}
