package dev.f012.billowars.listener;

import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeInstance;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 * Listener for general events.
 */
public class GeneralListener implements Listener {

  @EventHandler
  public void onPlayerJoin(PlayerJoinEvent event) {
    Player player = event.getPlayer();
    // Default Attack speed is 4. Set to 100 to effectively disable hit delay
    player.getAttribute(Attribute.GENERIC_ATTACK_SPEED).setBaseValue(100D);
  }

}
