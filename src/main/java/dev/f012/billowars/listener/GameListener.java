package dev.f012.billowars.listener;

import dev.f012.billowars.BilloWars;
import dev.f012.billowars.GameManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

/**
 * Listener for game related events.
 */
public class GameListener implements Listener {

  @EventHandler
  public void onEntityDamage(EntityDamageEvent event) {
    if (!(event.getEntity() instanceof Player)) return;
    if (!this.isGameStarted()) return;
    Player player = (Player) event.getEntity();

    // Player doesn't die (not interesting for us)
    if (player.getHealth() - event.getFinalDamage() > 0) return;

    GameManager gm = BilloWars.getInstance().getGameManager();
    event.setCancelled(true);

    gm.onPlayerDeath(player);
  }

  /**
   * @return whether the game is started.
   */
  private boolean isGameStarted() {
    return BilloWars.getInstance().isGameReady()
        && BilloWars.getInstance().getGameManager().isStarted();
  }

}
