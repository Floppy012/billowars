package dev.f012.billowars.gen;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.generator.BlockPopulator;
import org.bukkit.generator.ChunkGenerator;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Simple {@link ChunkGenerator} that generates an empty world with a single stone block at 0,32,0
 */
public class EmptyChunkGenerator extends ChunkGenerator {

  private static final List<BlockPopulator> EMPTY_BLOCK_POP = new ArrayList<>();
  private ChunkData empty;

  @Override
  public ChunkData generateChunkData(World world, Random random, int x, int z, BiomeGrid biome) {
    if (this.empty == null) {
      this.empty = super.createChunkData(world);
    }

    // Need something to stand on ...
    if (x == 0 && z == 0) {
      ChunkData cd = super.createChunkData(world);
      cd.setBlock(0, 31, 0, Material.STONE);
      return cd;
    }

    return this.empty;
  }

  @Override
  public boolean canSpawn(World world, int x, int z) {
    return true;
  }

  @Override
  public List<BlockPopulator> getDefaultPopulators(World world) {
    return EMPTY_BLOCK_POP;
  }

  @Override
  public Location getFixedSpawnLocation(World world, Random random) {
    return new Location(world, 0, 32,0);
  }

  @Override
  public boolean isParallelCapable() {
    return true;
  }
}
