package dev.f012.billowars;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import dev.f012.billowars.util.ItemStackAdapter;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Item;
import org.bukkit.inventory.ItemStack;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Configuration model for Game Map Configs.
 */
public class GameConfig {

  /**
   * GSON instance with pretty printing to allow
   * layer 8 to edit the config by hand.
   */
  private static transient final Gson GSON = new GsonBuilder()
      .setPrettyPrinting()
      .create();

  /**
   * Configuration File representation.
   */
  private transient File file;
  /**
   * Inventory cache so we don't need to re-deserialize it every player death.
   */
  private transient ItemStack[] cachedInventory;

  /**
   * Main spawn location of the game map.
   */
  public SimpleLocation spawn;
  /**
   * Possible Teams
   */
  public final Map<String, GameTeam> teams = new HashMap<>();
  /**
   * Raw inventory data.
   */
  public Map<String, Object>[] inventory = null;

  /**
   * Loads the configuration from file.
   * @param file the file to load the config from.
   * @return an instance of {@link GameConfig}.
   * @throws IOException if an error occurred when reading the file.
   */
  public static GameConfig load(File file) throws IOException {
    try (JsonReader reader = new JsonReader(new FileReader(file))){
      GameConfig config = GSON.fromJson(reader, GameConfig.class);
      config.file = file;
      return config;
    } catch (FileNotFoundException e) {
      return null;
    }
  }

  /**
   * @param file the file that this config will be saved to.
   * @return an empty {@link GameConfig} instance.
   */
  public static GameConfig empty(File file) {
    GameConfig out = new GameConfig();
    out.file = file;
    return out;
  }

  /**
   * Saves the values from this instance to the pre-defined file.
   * @throws IOException if an error occurs during file save.
   */
  public void save() throws IOException {
    if (!this.file.getParentFile().exists()) {
      if (!this.file.getParentFile().mkdirs()) {
        throw new IOException("Cannot create directories for game config");
      }
    }

    String json = GSON.toJson(this);
    try (FileWriter writer = new FileWriter(this.file, false)) {
      writer.write(json);
    }
  }

  /**
   * If absent, the cached inventory is computed and returned.
   * @return the cached inventory.
   */
  public ItemStack[] getCachedInventory() {
    if (this.cachedInventory == null) {
      this.cachedInventory = new ItemStack[this.inventory.length];
      for (int i = 0; i < this.inventory.length; i++) {
        Map in = this.inventory[i];
        if (in == null) continue;
        this.cachedInventory[i] = ItemStackAdapter.deserialize(in);
      }
    }

    return this.cachedInventory;
  }

  /**
   * Representation of a BilloWars Team in config.
   */
  @AllArgsConstructor
  @NoArgsConstructor
  public static class GameTeam {

    /**
     * Team spawn location
     */
    public SimpleLocation spawn;
    /**
     * Team bed location
     */
    public SimpleLocation bed;
    /**
     * Whether friendly fire is allowed
     */
    public boolean friendlyFire;

  }

  /**
   * Simplified world-independent location representation.
   */
  @AllArgsConstructor
  public static class SimpleLocation {
    public double x;
    public double y;
    public double z;
    public float pitch;
    public float yaw;

    /**
     * Converts this simplified location into a {@link Location} instance.
     * @param world the {@link World} to link this location to.
     * @return the {@link Location}
     */
    public Location toLocation(World world) {
      return new Location(world, this.x, this.y, this.z, this.yaw, this.pitch);
    }

    /**
     * Converts a {@link Location} to {@link SimpleLocation}.
     * @param loc the {@link Location} to convert.
     * @return the {@link SimpleLocation}
     */
    public static SimpleLocation fromLocation(Location loc) {
      return new SimpleLocation(loc.getX(), loc.getY(), loc.getZ(), loc.getPitch(), loc.getYaw());
    }
  }

}
