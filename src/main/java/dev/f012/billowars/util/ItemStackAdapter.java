package dev.f012.billowars.util;

import dev.f012.billowars.BilloWars;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import java.util.Map;
import java.util.logging.Level;

/**
 * Special {@link ItemStack} Adapter since there are some problems regarding the serialization of
 * {@link ItemStack}s. The {@link ItemStack#serialize()}-Method doesn't serialize the nested {@link ItemMeta}.
 * Because of this we need to (de-)serialize the {@link ItemMeta} ourselves. Because I want Layer 8 to be able to
 * change stuff in the config directly I chose the YAML stuff. Pretty ugly but what do you expect from a plugin called
 * BilloWars ("Billo" = "Cheap") ...
 */
public class ItemStackAdapter {

  /**
   * Deserializes a {@link Map} that was serialized using this adapter.
   * @param map the map to deserialize
   * @return the {@link ItemStack} that was deserialized.
   */
  public static ItemStack deserialize(Map<String, Object> map) {
    map.computeIfPresent("meta", (key, meta) -> {
      YamlConfiguration config = new YamlConfiguration();
      try {
        config.loadFromString((String) meta);
        return (ItemMeta) config.get("meta");
      } catch (InvalidConfigurationException e) {
        BilloWars.getInstance().getLogger().log(Level.SEVERE, "Error while deserializing ItemMeta", e);
        return null;
      }
    });

    return ItemStack.deserialize(map);
  }


  /**
   * Serializes an {@link ItemStack} with its {@link ItemMeta}.
   * @param src the {@link ItemStack} to serialize.
   * @return a map representing the proper serialized {@link ItemStack}.
   */
  public static Map<String, Object> serialize(ItemStack src) {
    Map<String, Object> map = src.serialize();
    map.computeIfPresent("meta", (key, meta) -> {
      YamlConfiguration config = new YamlConfiguration();
      config.set("meta", meta);
      return config.saveToString();
    });
    return map;
  }
}
