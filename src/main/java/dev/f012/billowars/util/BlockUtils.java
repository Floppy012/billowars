package dev.f012.billowars.util;

import org.bukkit.block.Block;

/**
 * Utility regarding {@link Block}s
 */
public class BlockUtils {

  /**
   * Checks whether the provided block is a bed.
   * @param block the block.
   * @return <i>true</i> if the block is a bed, <i>false</i> otherwise.
   */
  public static boolean isBed(Block block) {
    switch (block.getType()) {
      case BLACK_BED:
      case BLUE_BED:
      case BROWN_BED:
      case CYAN_BED:
      case RED_BED:
      case GRAY_BED:
      case GREEN_BED:
      case LIGHT_BLUE_BED:
      case LIGHT_GRAY_BED:
      case LIME_BED:
      case MAGENTA_BED:
      case ORANGE_BED:
      case PINK_BED:
      case PURPLE_BED:
      case WHITE_BED:
      case YELLOW_BED:
        return true;
      default:
        return false;
    }
  }

}
